import torch
import torch.distributions as dist

# Probabilities for each category (must sum to 1)
probs = torch.tensor([0.2, 0.3, 0.5])  # Example probabilities

# Create a categorical distribution
categorical_dist = dist.Categorical(probs=probs)

# Sample from the distribution
samples = categorical_dist.sample(sample_shape=(10,)) # Get 10 samples
print(samples) # Output will be category indices (0, 1, or 2 in this case)


samples = samples.float()

mean, std = torch.mean(samples), torch.std(samples)
distribute = torch.distributions.Normal(mean, std)


# Calculate the KL divergence

print(dist.kl_divergence(distribute, distribute))


# Calculate probabilities (log_prob for numerical stability)
probs = categorical_dist.log_prob(samples)
print(probs)
