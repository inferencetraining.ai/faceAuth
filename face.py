import os
import cv2
from insightface.app import FaceAnalysis
import torch

import torch.nn.functional as functional

import torch.distributions as dist
import torch.nn as neural_network



# prompt: compare face embediggs




class FaceRec:
    def __init__(self):
        self.foldername = '../lfw/Zhang_Ziyi/'
        self.files = []
        self.files_attempt = []
        self.embeds = []
        self.diff = []
        self.ground_mathches = []
        self.sample_true = []
        self.sample_attemt = []
        self.folder_attempt='../lfw/Zhang_Ziyi/'
        self.folder_ground = '../lfw/Zhang_Ziyi/'
        self.folder_camera = '../lfw/Zhang_Ziyi/'
        self.files_ground = [self.folder_ground+files for files in os.listdir(self.folder_ground)][0::1]
        self.files_attempt = [self.folder_attempt+files for files in os.listdir(self.folder_attempt)][2::]
        self.files_camera = [self.folder_camera+files for files in os.listdir(self.folder_camera)][0::1]
        self.zip_ground = list(zip(self.files_ground, self.files_attempt))
        self.zip_attempt = list(zip(self.files_attempt, self.files_camera))


        
    
    def embeddings(self, image):
        app = FaceAnalysis(name="buffalo_l", providers=['CPUExecutionProvider'])
        app.prepare(ctx_id=0, det_size=(640, 640))
        image1 = cv2.imread(image)
        faces = app.get(image1)
        
        faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
        return(torch.Tensor(faceid_embeds))



    def face_embed(self, face, face1):
        # Load the two images and get their face embeddings.
        face_encodings = self.embeddings(face)
        face_encodings1 = self.embeddings(face1)
        return(torch.nn.functional.cosine_similarity(face_encodings, face_encodings1))
    


    
    

    def expectation(self, sample_data):
        counts = sample_data  # Counts of each category
        probs = counts / counts.sum()
        soft = neural_network.Softmax(dim=0)
        categorical_dist = dist.Categorical(probs=probs)
        return(categorical_dist)




    def sim_distribution(self):
        attempt_embeddings = self.zip_ground[0::]
        ground_embeddings = self.zip_attempt[len(self.zip_ground)::]
        
        

        w_r_t_g = self.zip_ground[0::]
        w_r_t_c = self.zip_attempt

        w_r_t_g = self.zip_ground[0::len(self.zip_ground)//2]
        w_r_t_tr = self.zip_ground[len(self.zip_ground)//2::]


        

        ground_embeddings = [self.face_embed(attempting, attempt) for attempting, attempt in w_r_t_g]
        attempt_ground = [self.face_embed(attempting, attempt) for attempting, attempt in w_r_t_tr]


        ground_embeddings_g = [self.face_embed(attempting, attempt) for attempting, attempt in w_r_t_g]
        attempt_ground_c = [self.face_embed(attempting, attempt) for attempting, attempt in w_r_t_c]

        print(ground_embeddings, 't')
                
        self.sampling_ground = self.expectation(torch.Tensor(ground_embeddings))
        self.sampling_attempt_g = self.expectation(torch.Tensor(attempt_ground))


        self.sampling_ground = self.expectation(torch.Tensor(ground_embeddings_g))
        self.sampling_attempt_c = self.expectation(torch.Tensor(attempt_ground_c))

        return(self.sampling_ground, self.sampling_attempt_g, self.sampling_attempt_c)
    

    
    def model(self):       
        sim_distribution = self.sim_distribution()
        sim_kl = dist.kl_divergence(sim_distribution[0], sim_distribution[2])
        tol_epsilon = 1e-6  # A small positive value
        assert sim_kl <= tol_epsilon
        


       



Recognition = FaceRec()
print(Recognition.model())


