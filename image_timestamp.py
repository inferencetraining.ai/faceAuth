from PIL import Image
from PIL.ExifTags import TAGS

def get_image_timestamp(image_path):
  """
  Extracts the timestamp from an image's EXIF data.

  Args:
    image_path: The path to the image file.

  Returns:
    A string representing the timestamp, or None if no timestamp is found.
  """
  try:
    img = Image.open(image_path)
    exif_data = img._getexif()

    if exif_data is not None:
      for tag_id, value in exif_data.items():
        tag = TAGS.get(tag_id, tag_id)
        if tag == 'DateTimeOriginal':
          return value
    return None
  except (AttributeError, KeyError, IOError):
    return None

# Example usage:
image_path = 'face.jpg'  # Replace with the actual path
timestamp = get_image_timestamp(image_path)

if timestamp:
  print(f"Image timestamp: {timestamp}")
else:
  print("Timestamp not found in the image.")