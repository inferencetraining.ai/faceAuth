sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3-venv
python3 -m venv FaceAuth

source FaceAuth/bin/activate

pip3 install -r requirements.txt