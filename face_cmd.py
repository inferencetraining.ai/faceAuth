from insightface.app import FaceAnalysis
import face_recognition
import cv2
import os
import random

import numpy as np
import sys

def embeddings(image):
    app = FaceAnalysis(name="buffalo_l", providers=['CPUExecutionProvider'])
    app.prepare(ctx_id=0, det_size=(640, 640))
    image1 = cv2.imread(image)
    faces = app.get(image1)

    faceid_embeds = (faces[0].normed_embedding)
    return(faceid_embeds)

def face_verify(facex1, facex2):
    facex1 = embeddings(facex1)
    known_face_encoding = embeddings(facex2)

    matches = face_recognition.compare_faces([known_face_encoding], facex1, tolerance=0.85)
    
    if bool(matches[0]) == True:
       print('yes')

       random_seed =  [1,4,5]
       passwd = [facex1[rand] for rand in random_seed]
       passwd = passwd[0]

      #
       command = 'bash scripts.sh'
       os.system("echo %s | sudo -S %s"%(passwd, command))

    return(matches)




face_verify('facex1.jpg', 'facex11.jpg')
