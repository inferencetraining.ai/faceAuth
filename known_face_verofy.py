from insightface.app import FaceAnalysis
import face_recognition
import cv2

import random

import sys

def embeddings(image):
    app = FaceAnalysis(name="buffalo_l", providers=['CPUExecutionProvider'])
    app.prepare(ctx_id=0, det_size=(640, 640))
    image1 = cv2.imread(image)
    faces = app.get(image1)

    faceid_embeds = (faces[0].normed_embedding)
    return(faceid_embeds)

def face_verify(facex1, facex2):
    facex1 = embeddings(facex1)
    known_face_encoding = embeddings(facex2)

    matches = face_recognition.compare_faces([known_face_encoding], facex1)
    random_seed = []
    passwd = [facex1[rand] for rand in random_seed]
    command = sys.argv[1]
    os.system("echo %s | sudo -S python3 %s"%(command, passwd))

    return(matches)




